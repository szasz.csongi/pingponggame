public class VariableStepGameLoop extends GameLoop{

    public VariableStepGameLoop(GamePanel gamePanel) {
        super(gamePanel);
    }

    @Override
    protected void processGameLoop() {
        long lastTime = System.nanoTime();
        double amountOfTicks =120.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        while(isGameRunning()) {
            long now = System.nanoTime();
            delta += (now -lastTime)/ns;
            lastTime = now;
            if(delta >=1) {
                update();
                render();
                delta--;
            }
        }
    }

    protected void update() {
        this.gamePanel.move();
        this.gamePanel.checkCollision();
    }
}
