import java.awt.*;
import java.awt.event.KeyEvent;

public class Paddle extends Rectangle{
    int id;
    int yVelocity;
    int speed = 10;
    Paddle(int x, int y, int PADDLE_WIDTH, int PADDLE_HEIGHT, int id) {
        super(x,y,PADDLE_WIDTH,PADDLE_HEIGHT);
        this.id=id;
    }

    public void keyPressed(KeyEvent e){
        switch (id) {
            case 0:
                if(e.getKeyCode() == KeyEvent.VK_W) {
                    setYDirection(-speed);
//                    System.out.println("Megy");
                }
                if(e.getKeyCode() == KeyEvent.VK_S) {
                    setYDirection(speed);
//                    System.out.println("Megy");
                }
                break;
            case 1:
                if(e.getKeyCode() == KeyEvent.VK_O) {
                    setYDirection(-speed);
//                    System.out.println("Megy");

                }
                if(e.getKeyCode() == KeyEvent.VK_L) {
                    setYDirection(speed);
//                    System.out.println("Megy");
                }
                break;
        }
    }

    public void keyReleased(KeyEvent e){
        switch (id) {
            case 0:
                if(e.getKeyCode() == KeyEvent.VK_W) {
                    setYDirection(0);
                }
                if(e.getKeyCode() == KeyEvent.VK_S) {
                    setYDirection(0);
                }
                break;
            case 1:
                if(e.getKeyCode() == KeyEvent.VK_O) {
                    setYDirection(0);
                }
                if(e.getKeyCode() == KeyEvent.VK_L) {
                    setYDirection(0);
                }
                break;
        }
    }

    public void setYDirection(int yDirection) {
        yVelocity = yDirection;
    }

    public void move() {
        y = y + yVelocity;
    }

    public void draw(Graphics g){
        if(id == 0) {
            g.setColor(Color.red);
        } else {
            g.setColor(Color.green);
        }
        g.fillRect(x,y,width,height);
    }
}
