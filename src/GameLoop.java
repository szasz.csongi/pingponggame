import java.util.Random;

public abstract class GameLoop{

    protected volatile GameStatus status;

    protected GameController controller;

    private Thread gameThread;
    public GamePanel gamePanel;

    public GameLoop(GamePanel gamePanel) {
        status = GameStatus.STOPPED;
        this.gamePanel = gamePanel;
    }

    public void run() {
        status = GameStatus.RUNNING;
        gameThread = new Thread(this::processGameLoop);
        gameThread.start();
    }

    public void stop() {
        status = GameStatus.STOPPED;
    }

    public boolean isGameRunning() {
        return status == GameStatus.RUNNING;
    }

    protected void render() {
        this.gamePanel.repaint();
    }

    protected abstract void processGameLoop();
}
