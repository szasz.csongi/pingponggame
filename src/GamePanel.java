import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

public class GamePanel extends JPanel{

    static final int WIDTH = 1000;
    static final int HEIGHT = (int)(WIDTH * (0.555));
    static final Dimension SCREEN_SIZE = new Dimension(WIDTH, HEIGHT);
    static final int BALL_DIAMETER = 20;
    static final int PADDLE_WIDTH = 25;
    static final int PADDLE_HEIGHT = 100;

    private Image image;
    private Graphics graphics;
    private Paddle paddle1;
    private Paddle paddle2;

    Ball ball;
    public GamePanel(){
        newBall();
        newPaddles();
        this.setFocusable(true);
        this.addKeyListener(new ActionListener());
        this.setPreferredSize(SCREEN_SIZE);
    }

    public void newBall(){
        ball = new Ball((WIDTH/2) - (BALL_DIAMETER/2), (HEIGHT/2) - (BALL_DIAMETER/2), BALL_DIAMETER, BALL_DIAMETER);
    }

    public void newPaddles(){
        paddle1 = new Paddle(0,(HEIGHT/2)-(PADDLE_HEIGHT/2),PADDLE_WIDTH,PADDLE_HEIGHT,0);
        paddle2 = new Paddle(WIDTH-PADDLE_WIDTH,(HEIGHT/2)-(PADDLE_HEIGHT/2),PADDLE_WIDTH,PADDLE_HEIGHT,1);
    }

    public void paint(Graphics g) {
        image = createImage(getWidth(), getHeight());
        graphics = image.getGraphics();
        draw(graphics);
        g.drawImage(image,0,0,this);

    }

    public void draw(Graphics g) {
        paddle1.draw(g);
        paddle2.draw(g);
        ball.draw(g);
    }

    public void move() {
        paddle1.move();
        paddle2.move();
        ball.move();
//        System.out.println("Bent");
    }

    public void checkCollision() {
        //Fent vagy lent az uto
        if(paddle1.y<=0) {
            paddle1.y=0;
        }
        if(paddle1.y>=HEIGHT-PADDLE_HEIGHT) {
            paddle1.y = HEIGHT-PADDLE_HEIGHT;
        }

        if(paddle2.y<=0) {
            paddle2.y=0;
        }
        if(paddle2.y>=HEIGHT-PADDLE_HEIGHT) {
            paddle2.y = HEIGHT-PADDLE_HEIGHT;
        }

        //Labda fent vagy lent pattan
        if (ball.y <= 0){
            ball.setYDirection(-ball.yVelocity);
        }
        if (ball.y >= HEIGHT - BALL_DIAMETER){
            ball.setYDirection(-ball.yVelocity);
        }

        //Labda utorol pattan
        if(ball.intersects(paddle1)) {
            ball.setXDirection(-ball.xVelocity);
            ball.xVelocity++;
            if(ball.yVelocity>=0){
                ball.yVelocity++;
            } else {
                ball.yVelocity--;
            }
        }

        if(ball.intersects(paddle2)) {
            ball.setXDirection(-ball.xVelocity);
            ball.xVelocity--;
            if(ball.yVelocity>=0){
                ball.yVelocity++;
            } else {
                ball.yVelocity--;
            }
        }

        //Vesztes
        if(ball.x <= 0){
            newBall();
            newPaddles();
        }

        if(ball.x >= WIDTH-BALL_DIAMETER) {
            newBall();
            newPaddles();
        }
    }

    public class ActionListener extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            paddle1.keyPressed(e);
            paddle2.keyPressed(e);
        }
        public void keyReleased(KeyEvent e) {
            paddle1.keyReleased(e);
            paddle2.keyReleased(e);
        }
    }

}
