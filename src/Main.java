// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        GameFrame frame = new GameFrame();
//        try {
            var frameBasedGameLoop = new VariableStepGameLoop(frame.getGamePanel());
            frameBasedGameLoop.run();
//            Thread.sleep(20000);
//            frameBasedGameLoop.stop();
//        } catch (InterruptedException e) {
//            System.out.println(e.getMessage());
//        }
    }
}